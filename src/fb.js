var $ = require("jquery");
var shareUrl = 'http://uk-microsites.ign.com/fallout-4';
var Share = require('./share');

function Fb () {

   this.$window = $(window);
   this.$window.on('load', $.proxy(this.init, this));

};

Fb.prototype.init = function(){

   new Share(".share_btn", {
     ui: {
       flyout: "bottom center"
     },
     networks: {
       facebook: {
         app_id: "543961095774853",
         url: shareUrl
       },
       google_plus: {
         url: shareUrl
       },
       twitter: {
         url: shareUrl
       },
       pinterest: {
         url: shareUrl
       }
     }
   });

};

module.exports = Fb;
