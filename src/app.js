var $ = jQuery = require("jquery");
//var Packery = require("packery");
//var IsoPackery = require("isotope-packery");
//var Isotope = require("isotope-layout");

// var Packery = require("./packery.js");
// var Isotope = require("./isotope.js");

var Fb = require("./fb");



$(document).ready(function () {


    var FB = new Fb();

    // entry-section COMPETITION / PRE-ORDER selection
    $('.Entry-option').hover(function () {
        if ($(this).hasClass('Entry-option--active') === false) {
            $('.Entry-option--active').removeClass('Entry-option--active');
            $(this).addClass('Entry-option--active');
        }
    });

    // scroll to form
    $('.Entry-enter, a.enter-competition').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('.Page-form').offset().top - 55
        }, 300);
    });

    // Terms links
    $(document).on('click', '.terms-link', function(e) {
        e.preventDefault();

        var url = $(this).attr('href');
        window.open(url, "Terms and Conditions", "width=600,height=600,menubar=no");
    });

    // Videos
    $(document).on('click', '[data-video=true]', function(e) {
        e.preventDefault();

        var $lightbox = $('#videoLightbox'),
            $iframe = $('#videoPlayer'),
            $content = $('#lightboxContent');

        var offsetTop = $(this).offset().top;
        var vidUrl = $(this).attr('href');
        var isMobile = ($(window).width() < 768);

        if (isMobile) {
            $content.css('margin-top', (offsetTop - 100));
        }

        $iframe.attr('src', 'http://widgets.ign.com/video/embed/content.html?url='+vidUrl);

        $lightbox.fadeIn(300)
            .on('click', function(e) {
                closeLightbox();
            })
            .on('click', '.content', function(e) {
                e.stopPropagation();
            })
            .on('click', '.close', function(e) {
                e.preventDefault();
                closeLightbox();
            });

        var closeLightbox = function() {
            $lightbox.fadeOut(300, function() {
                $iframe.attr('src', '');

                if (isMobile) {
                    $content.removeAttr('style');
                }
            });

        };
    });


    // Form
    var $error = false;

    var formError = function(message) {
        if (!$error)
            $error = $('<p class="error"></p>');

        $error
            .html(message)
            .insertBefore($('#submitForm').parent())
            .hide()
            .slideDown(200);
    };

    $('#submitForm').on('click', function(e) {
        e.preventDefault();

        var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // Get form data
        var fd = {
            answer: $('input[name=answers]:checked').val(),
            first_name: $('input[name=fname]').val(),
            last_name: $('input[name=lname]').val(),
            email: $('input[name=email]').val(),
            // dob_d: $('select[name=dob_d]').val(),
            // dob_m: $('select[name=dob_m]').val(),
            // dob_y: $('select[name=dob_y]').val(),
            terms: ($('input[name=terms]').is(':checked')) ? 1 : 0,
            subscribe: ($('input[name=subscribe]').is(':checked')) ? 1 : 0
        };

        if (!fd.answer)
            return formError("Please select an answer");

        if (!fd.first_name)
            return formError("Please enter your first name");

        if (!fd.last_name)
            return formError("Please enter your surname");

        if (!fd.email)
            return formError("Please enter your email address");

        if (!fd.email.match(emailRegex))
            return formError("Your email address is invalid");

        // if (!fd.dob_d || !fd.dob_m || !fd.dob_y)
        //     return formError("Please enter your date of birth");

        if (!fd.terms)
            return formError("You must accept the terms & conditions");

        // If there are no validation errors
        if ($error) {
            $error.slideUp(200, function() {
                $error.remove();
                $error = false;
            });
        }

        // Submit the form
        var promise = $.ajax({
          url: 'lib/submit.php',
          dataType: 'json',
          type: 'POST',
          data: fd
        });

        promise.done(function(data) {
            if (data.success) {
                $('<p class="success"></p>')
                    .html("Thanks for entering!")
                    .insertBefore('#submitForm')
                    .hide()
                    .slideDown(200);

                $('#submitForm').fadeOut(200);
                $('#theForm')[0].reset();
            }
            else {
                formError(data.error);
            }
        });
    });



    // Parallax BG
    // -----------
    var scrollTop = $(window).scrollTop(),
        redraw = true,
        bg = document.getElementById('pageBackground');

    $(window).on('scroll', function(e) {
        scrollTop = $(this).scrollTop();
        redraw = true;
    });

    var positionBG = function(top) {
        bg.style.transform = 'translate3d(0, '+top+'px, 0)';
        bg.style.OTransform = 'translate3d(0, '+top+'px, 0)';
        bg.style.msTransform = 'translate3d(0, '+top+'px, 0)';
        bg.style.MozTransform = 'translate3d(0, '+top+'px, 0)';
        bg.style.WebkitTransform = 'translate3d(0, '+top+'px, 0)';
    };

    var step = function(timestamp) {
        if (redraw) {
            positionBG(Math.round(scrollTop / 3));
            redraw = false;
        }

        window.requestAnimationFrame(step);
    };

    window.requestAnimationFrame(step);


    // Map
    // ---
    var $map = $('#levelMap');

    $map.on('click', 'a', function(e) {
        e.preventDefault();
        var level = $(this).attr('data-sort');
        $map.attr('class', 'map map-'+level);
        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');
    });


    // Geolocation
    // -----------
    var country = window._ZDGEOCOUNTRY;
    // var country = 'GB';

    if(country == 'GB'){
        $('._au, ._other').each(function(i, el) {
            if (!$(this).hasClass('_gb'))
                $(this).hide();
        });
    }

    if(country == 'AU'){
        $('._gb, ._other').each(function(i, el) {
            if (!$(this).hasClass('_au'))
                $(this).hide();
        });

        $('#formContainer').addClass('Page-form-au');
    }

    if(country != 'AU' && country != 'GB'){
        $('._au, ._gb').each(function(i, el) {
            if (!$(this).hasClass('_other'))
                $(this).hide();
        });
    }

    // Remove loading styles
    $('style#countries').remove();


    // Fixed nav
    // ---------
    var navLinksOn = false;

    $(window).on('scroll', function(e) {
        var scrollTop = $(window).scrollTop();

        if (scrollTop >= 70 && !navLinksOn) {
            $('#topNav').addClass('show-links');
            navLinksOn = true;
        }
        else if (scrollTop < 70 && navLinksOn) {
            $('#topNav').removeClass('show-links');
            navLinksOn = false;
        }
    });

});


// shim layer with setTimeout fallback
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();
