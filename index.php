<?php include_once('./lib/functions.php') ?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fallout 4 IGN Microsite</title>
    <meta name="description" content="Keep up to date with everything Fallout 4 right here, including new DLC, cool mods, and more.">
    <link rel="shortcut icon" href="/favicon.png">
    <link rel="apple-touch-icon" href="dist/build/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="dist/build/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="dist/build/img/apple-touch-icon-114x114.png">
    <!-- Open Graph-->
    <meta property="og:title" content="Fallout 4 IGN Microsite">
    <meta property="og:site_name" content="Keep up to date with everything Fallout 4 right here, including new DLC, cool mods, and more.">
    <meta property="og:url" content="http://uk-microsites.ign.com/fallout-4">
    <meta property="og:image" content="http://uk-microsites.ign.com/fallout-4/dist/build/img/ogimage.jpg">
    <meta property="og:description" content="Keep up to date with everything Fallout 4 right here, including new DLC, cool mods, and more.">
    <meta property="fb:app_id" content="543961095774853">
    <link rel="stylesheet" href="dist/build/css/style.css">

    <script src="https://use.typekit.net/rqb4eew.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <style id="countries" type="text/css">._gb, ._au, ._other { display: none!important; }</style>
  </head>
  <body>
    <div data-offcanvas class="off-canvas-wrap">
      <div class="inner-wrap">
        <nav id="topNav" data-topbar role="navigation" class="tab-bar tab-bar-red top">
          <section class="left-small show-for-small-only"><a href="#" class="left-off-canvas-toggle menu-icon"><span></span></a></section>
          <div class="row">
            <div class="ign_red-wrapper">
              <h1 class="title"><a href="http://www.ign.com" target="_blank"><img src="dist/build/img/boilerplate/ign-logo.png" alt="IGN logo"></a></h1>
            </div>
            <ul id="share_override" class="left share_btn">
              <li><a href="#"></a></li>
            </ul><span class="sponsored">Sponsored</span>
            <ul class="right links">
              <li><a href="https://fallout4.com/buy-now#automatron" target="_blank" class="_other" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click']);">Buy Fallout 4</a></li>
            </ul>
          </div>
        </nav>
        <div id="pageBackground" class="Page-background"></div>




        <div class="Page">
          <div class="Page-entry">

            <div class="Center Center-height">

              <div class="Entry-logo"><img src="dist/build/img/fallout-assets/INST_2DLogo_Vector-White.svg" alt="" /></div>

              <div id="levelMap" class="map map-noob">
                <div class="Entry-desc">

                    <p class="Entry-text">Can’t get enough of Fallout 4? Us neither.</p>
                    <p class="Entry-text">We’ve created this page to help you stay up to date with everything from new DLC to cool mods and recently discovered locations.</p>
                    <p class="Entry-text">Whether you’re new to Fallout or a seasoned player, you can filter content to match your level of experience by selecting one of the buttons below.</p>

                </div>

                <ul class="switch">
                  <li class="active"><a href="#" onClick="_gaq.push(['_trackEvent', 'ds_noob', 'click']);" data-sort="noob">I’ve not played Fallout 4 before</a></li>
                  <li><a href="#" onClick="_gaq.push(['_trackEvent', 'ds_average', 'click']);" data-sort="player">I’ve played Fallout 4 before</a></li>
                  <li><a href="#" onClick="_gaq.push(['_trackEvent', 'ds_pro', 'click']);" data-sort="pro">I’m really good at all-things Fallout 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!--  grid-->
          <div class="Page-grid Center">

              <div class="pre-order">
                  <a href="https://fallout4.com/buy-now#automatron" target="_blank" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click']);"><img src="dist/build/img/fallout-assets/packshots.png" alt="Buy Fallout 4" /></a>
                  <a href="https://fallout4.com/buy-now#automatron" target="_blank" class="ghostbutton">buy fallout 4</a>
              </div><a target="_blank" data-pro="0.1" data-player="0.01" data-noob="0.01" href="http://uk.ign.com/articles/2016/05/17/nature-reclaims-the-wasteland-in-this-fallout-4-mod-overhaul" class="Grid-item Grid-item--big Grid-reclaims">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Nature Reclaims The Wasteland in This Fallout 4 Mod Overhaul</h2>
                <div class="Grid-text">Best of British.</div>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.1" data-noob="0.01" data-video="true" href="http://uk.ign.com/videos/2016/05/19/the-first-20-minutes-of-fallout-4-far-harbor-dlc" class="Grid-item Grid-item--small Grid-first20" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">First 20 Minutes of Fallout 4: Far Harbor DLC</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.01" data-noob="0.01" data-video="true" href="http://uk.ign.com/maps/fallout-4/the-island" class="Grid-item Grid-item--small Grid-interactivemap">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Find Everything in This Interactive Map of Fallout 4's Far Harbor DLC</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.01" data-noob="0.01" data-video="true" href="http://uk.ign.com/videos/2016/05/19/fallout-4-far-harbor-dlc-islanders-almanac-magazine-locations" class="Grid-item Grid-item--small Grid-magazines">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Locate Every Magazine in Fallout 4's Far Harbor DLC</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.1" data-noob="0.01" data-video="true" href="http://uk.ign.com/videos/2016/05/18/fallout-4-official-exploring-far-harbor" class="Grid-item Grid-item--big Grid-exploring" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Fallout 4 Exploring Far Harbor</h2>
                <div class="Grid-text">Best of British.</div>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.01" data-noob="0.01" data-video="true" href="http://uk.ign.com/videos/2016/05/19/this-fallout-4-resurrection-mod-is-completely-stunning" class="Grid-item Grid-item--small Grid-resurrectionmod">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">This Fallout 4 Resurrection Mod is Completely Stunning</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.01" data-noob="0.01" href="http://uk.ign.com/articles/2016/05/19/ign-plays-live-fallout-4-far-harbor-dlc" class="Grid-item Grid-item--small Grid-harbourlive">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">IGN Plays Live: Fallout 4: Far Harbor DLC</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.01" data-noob="0.01" href="http://uk.ign.com/articles/2016/04/08/bafta-games-awards-2016-winners-revealed" class="Grid-item Grid-item--big Grid-bafta">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">Fallout 4 Takes Top Prize at BAFTA Games Awards 2016</h2>
                <div class="Grid-text">Best of British.</div>
            </div></a><a target="_blank" data-pro="0.15" data-player="0.1" data-noob="50" href="http://uk.ign.com/videos/2016/04/05/fallout-4-official-wasteland-workshop-trailer" data-video="true" class="Grid-item Grid-item--small Grid-workshop2">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--trailer">
                <h3 class="Grid-category">Trailer</h3>
                <h2 class="Grid-title">Fallout 4 Wasteland Workshop DLC Trailer</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.03" data-noob="0.06" href="http://uk.ign.com/articles/2016/04/07/fallout-4s-survival-mode-gets-more-balancing-tweaks" class="Grid-item Grid-item--small Grid-survival">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">Fallout 4's Survival Mode Gets More Balancing Tweaks</h2>
            </div></a><a target="_blank" data-pro="0.02" data-player="0.01" data-noob="0.01" href="http://uk.ign.com/articles/2016/04/05/fallout-4s-second-dlc-wasteland-workshop-gets-a-release-date" class="Grid-item Grid-item--small Grid-workshop">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">Fallout 4's Second DLC Gets A Release Date</h2>
            </div></a><a target="_blank" data-pro="1" data-player="2" data-noob="50" href="http://uk.ign.com/wikis/fallout-4/Automatron_Pip_Boy_Minigame" class="Grid-item Grid-item--small Grid-minigame">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Fallout 4's Hidden Twin-Stick Shooter</h2>
            </div></a><a target="_blank" data-pro="0.1" data-player="0.1" data-noob="0.1" href="http://uk.ign.com/articles/2016/03/24/fallout-4-automatron-dlc-review?watch" class="Grid-item Grid-item--big Grid-dlcreview">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--review">
                <h3 class="Grid-category">Review</h3>
                <h2 class="Grid-title">Fallout 4 Automatron DLC Review</h2>
                <div class="Grid-text">The droids you're looking for.</div>
            </div></a><a target="_blank" data-pro="0.2" data-player="1" data-noob="50" href="http://uk.ign.com/wikis/fallout-4/Ballistic_Weave" class="Grid-item Grid-item--big Grid-boss">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Get the Best Armour for Automatron's Toughest Robots</h2>
                <div class="Grid-text">This method of protection can provide a player with defenses in excess of 220 physical and 220 energy.</div>
            </div></a><a target="_blank" data-pro="0.5" data-player="0.1" data-noob="50" href="http://uk.ign.com/videos/2016/03/22/fallout-4-automatron-find-out-who-is-behind-all-this-robot-madness" class="Grid-item Grid-item--small Grid-madness">
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">Find Out Who Is Behind All This Robot Madness in Automatron</h2>
          </div></a><a target="_blank" data-pro="0.6" data-player="0.2" data-noob="50" href="http://uk.ign.com/articles/2016/03/18/captain-america-civil-war-trailer-recreated-in-fallout-4" class="Grid-item Grid-item--small Grid-captain">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Captain America: Civil War Trailer Recreated in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="50" href="http://uk.ign.com/articles/2016/03/22/fallout-4-survival-mode-to-enter-beta-on-pc-soon" class="Grid-item Grid-item--small Grid-beta">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">Fallout 4 Survival Mode Now in Beta on PC</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2016/03/23/modding-curie-in-fallout-4s-automatron" class="Grid-item Grid-item--small Grid-curie" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Modding Curie in Fallout 4's Automatron</h2>
            </div></a><a target="_blank" data-pro="1" data-player="0.3" data-noob="50" href="http://uk.ign.com/articles/2016/03/14/tons-of-images-from-the-fallout-4-automatron-trailer" class="Grid-item Grid-item--small Grid-opinion">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--gallery">
                <h3 class="Grid-category">Gallery</h3>
                <h2 class="Grid-title">34 Images from the Fallout 4 Automatron Trailer</h2>
            </div></a><a target="_blank" data-pro="1.5" data-player="1.5" data-noob="1" href="http://uk.ign.com/videos/2016/03/15/fallout-4-dlc-automatron-trailer-breakdown" class="Grid-item Grid-item--big Grid-throne" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Fallout 4 Automatron DLC Trailer Breakdown</h2>
                <div class="Grid-text">What on earth is happening in the new Automatron Fallout expansion?</div>
            </div></a><a target="_blank" data-pro="1.2" data-player="1.2" data-noob="1.2" href="http://uk.ign.com/articles/2016/02/16/first-series-of-add-ons-revealed-for-fallout-4" class="Grid-item Grid-item--small Grid-levelUp">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">First Series of Add-ons Revealed for Fallout 4</h2>
            </div></a><a target="_blank" data-pro="1.1" data-player="1.1" data-noob="1.1" href="http://uk.ign.com/wikis/fallout-4/Books_and_Magazines" class="Grid-item Grid-item--small Grid-chooseClass">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Every Magazine and Skill Book in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="1.3" data-player="1.3" data-noob="1.3" href="http://uk.ign.com/wikis/fallout-4/Companions" class="Grid-item Grid-item--small Grid-item--margin Grid-getLevel">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">How to Find Every Companion in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="50" data-player="60" data-noob="1" href="http://uk.ign.com/wikis/fallout-4/Absolute_Beginner_Starter_Guide" class="Grid-item Grid-item--small Grid-item--margin Grid-starterpack">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">A Guide For Someone Who's Never Played Fallout</h2>
            </div></a><a target="_blank" data-pro="70" data-player="70" data-noob="0.1" href="http://uk.ign.com/wikis/fallout-4/Things_to_Do_First" class="Grid-item Grid-item--big Grid-grinding">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">11 Things To Do First in Fallout 4</h2>
                <div class="Grid-text">Fallout 4 is huge, but this day one wiki guide will walk you through every hidden item and secret you want to find on the first day.</div>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2016/03/14/fallout-4-official-automatron-trailer" data-video="true" class="Grid-item Grid-item--small Grid-taken">
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--trailer Grid-desc--maxi">
              <h3 class="Grid-category">Trailer</h3>
              <h2 class="Grid-title">Fallout 4 Official Automatron DLC Trailer</h2>
          </div></a><a target="_blank" data-pro="0.8" data-player="0.7" data-noob="70" href="http://uk.ign.com/videos/2015/12/30/vault-why-we-keep-coming-back-to-fallout-4-vault-ign" data-video="true" class="Grid-item Grid-item--small Grid-coop">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--opinion">
                <h3 class="Grid-category">Opinion</h3>
                <h2 class="Grid-title">Vault: Why We Keep Coming Back To Fallout 4</h2>
            </div></a><a target="_blank" data-pro="11" data-player="11" data-noob="11" href="http://uk.ign.com/articles/2015/11/09/fallout-4-review" class="Grid-item Grid-item--big Grid-howTrials">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--review Grid-desc--maxi">
                <h3 class="Grid-category">Review</h3>
                <h2 class="Grid-title">Fallout 4 Review</h2>
                <div class="Grid-text">We’re giving this post-nuclear RPG a glowing endorsement.</div>
            </div></a><a target="_blank" data-pro="1" data-player="50" data-noob="50" href="http://www.ign.com/videos/2016/01/27/the-fallout-4-spoilercast-vault-ign" class="Grid-item Grid-item--small Grid-item--margin Grid-factions">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--opinion">
                <h3 class="Grid-category">Opinion</h3>
                <h2 class="Grid-title">Vault: The Fallout 4 Spoilercast</h2>
            </div></a><a target="_blank" data-pro="1.1" data-player="1.1" data-noob="1.1" href="http://uk.ign.com/wikis/fallout-4/Best_Weapons" class="Grid-item Grid-item--small Grid-fiveWays">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Best Weapons in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="50" href="http://uk.ign.com/articles/2016/01/01/fallouts-most-horrifying-vault-stories" class="Grid-item Grid-item--small Grid-101">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">7 of Fallout's Most Horrifying Vault Stories</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="50" href="http://uk.ign.com/wikis/fallout-4/Endings" class="Grid-item Grid-item--small Grid-level20">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">How Fallout 4's Multiple Endings Work</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/wikis/fallout-4/Vaults" class="Grid-item Grid-item--big Grid-bounties">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Every Vault Location in Fallout 4</h2>
                <div class="Grid-text">This page contains the Locations of and loot found in the 5 Vaults in Fallout 4's Commonwealth, as well as related Quests.</div>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/articles/2015/11/13/how-long-could-you-actually-survive-in-fallout"  class="Grid-item Grid-item--small Grid-xur">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">How Long Could You Actually Survive in Fallout?</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2015/11/09/7-stunning-fallout-4-timelapses-in-1080p-60fps" data-video="true" class="Grid-item Grid-item--big Grid-darkness">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">7 Stunning Fallout 4 Timelapses in 1080p 60fps</h2>
                <div class="Grid-text">Enjoy the stunning landscapes that Fallout 4 has to offer as we take you through a day / night cycle of some gorgeous locations.</div>
            </div></a><a target="_blank" data-pro="50" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2015/11/09/robot-and-human-love-in-fallout-4" data-video="true" class="Grid-item Grid-item--small Grid-vault">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Robot and Human Love in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="1" href="http://uk.ign.com/videos/2015/11/05/fallout-4-official-launch-trailer" data-video="true" class="Grid-item Grid-item--small Grid-compare">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--trailer Grid-desc--maxi">
                <h3 class="Grid-category">Trailer</h3>
                <h2 class="Grid-title">Fallout 4 Official Launch Trailer</h2>
            </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="1" href="http://uk.ign.com/wikis/fallout-4/Tradecraft" class="Grid-item Grid-item--small Grid-raids">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Grab One of Fallout 4's Best Weapons Early</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/articles/2015/11/12/you-can-find-the-bar-from-cheers-in-fallout-4" class="Grid-item Grid-item--small Grid-armour">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">You Can Find The Bar from Cheers in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="50" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2015/11/11/how-to-build-huge-buildings-in-fallout-4-ign-plays" data-video="true" class="Grid-item Grid-item--small Grid-players">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">How to Build Huge Buildings in Fallout 4</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2015/11/09/12-powerful-fallout-4-collectibles-and-weapons-to-grab-asap-best-way-to-play" data-video="true" class="Grid-item Grid-item--small Grid-raids2">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">12 Powerful Fallout 4 Collectibles and Weapons to Grab ASAP</h2>
            </div></a><a target="_blank" data-pro="60" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2015/11/09/fallout-4-get-power-armor-in-5-minutes-ign-plays" data-video="true" class="Grid-item Grid-item--small Grid-shirpa">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Fallout 4: Get Power Armor in 5 Minutes</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/videos/2015/10/22/fallout-in-5-minutes" data-video="true" class="Grid-item Grid-item--big Grid-making">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Fallout in 5 minutes</h2>
                <div class="Grid-text">Need a refresher before Fallout 4? This is everything you need to know about the story of Fallout.</div>
            </div></a></div>

             <div id="formContainer" class="Page-form Center">
                  <h2 class="Form-title">COMPETITION</h2>
                  <div class="comp-prizing">
                      <img src="dist/build/img/prizes.jpg" alt="competition prizing goes here" />
                  </div>
                  <div class="Form-info">
                    <div>
                        <p class="impact">WE’RE GIVING AWAY A SUBPAC AND FALLOUT 4 SEASON PASS TO FULLY IMMERSE YOUR EXPERIENCE WITHIN THE WASTELAND, ALONG WITH A FALLOUT 4 T-SHIRT AND HAT.</p>
                        <p>For your chance to win this prize, select the correct answer and then enter your details. Three runners-up will win a Fallout 4 Season Pass on a platform of choice, t-shirt and hat.</p>
                    </div>
                  </div>
                  <form id="theForm" class="Form-box">
                    <div>
                      <span class="Form-question">Which of these is not a Fallout 4 companion?</span>
                      <div class="checkboxes">
                        <label>
                          <input type="radio" name="answers" value="strong"><span>Strong</span>
                        </label>
                        <label>
                          <input type="radio" name="answers" value="piper"><span>Piper</span>
                        </label>
                        <label>
                          <input type="radio" name="answers" value="desdemona"><span>Desdemona</span>
                        </label>
                      </div>
                      <div class="form-row cols form-row-name">
                        <div class="col">
                          <input type="text" placeholder="First Name" name="fname">
                        </div>
                        <div class="col">
                          <input type="text" placeholder="Surname" name="lname">
                        </div>
                      </div>
                      <div class="form-row">
                        <input type="email" placeholder="Email" name="email">
                      </div>
                      <label class="checkbox">
                        <div class="col">
                          <input type="checkbox" name="terms" value="1">
                        </div>
                        <div class="col">I confirm I am a UK resident aged 18 or over and have read and understood the terms and conditions.</div>
                      </label>
                      <div class="cta">
                        <button type="submit" id="submitForm">Submit</button>
                      </div>
                    </div>
                    <div class="terms">
                      <div><a href="terms-gb.php" class="terms-link tk-futura-pt">Terms &amp; Conditions</a></div>
                    </div>
                  </form>
                </div>

        </div>


        <footer class="tab-bar">
          <div class="row">
            <div class="ign_red-wrapper wrapper-dark">
              <div class="title"><img src="dist/build/img/boilerplate/zd_logo.png" width="204" height="77"></div>
              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>
            <ul class="left links">

                <li>
                  <p>Copyright 2016</p>
                  <p class="nomargin">Ziff Davis International Ltd</p>
                </li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>

            </ul>
          </div>
        </footer><a class="exit-off-canvas"></a>
        <div id="videoLightbox" class="lightbox">
          <div id="lightboxContent" class="content">
            <div class="video-wrapper">
              <iframe id="videoPlayer" src="" width="468" height="263" scrolling="no" frameborder="0" allowfullscreen></iframe>
            </div><a href="#" class="close">&times;</a>
          </div>
        </div>

        <!-- <script type="text/javascript" src="http://geobeacon.ign.com/geodetect.js"></script> -->

        <script type="text/javascript" src="dist/build/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="dist/build/isotope.js"></script>
        <script type="text/javascript" src="dist/build/packery.js"></script>
        <script src="dist/build/build.min.js"></script>

        <script>

           $(document).ready(function(){
              var $grid = $('.Page-grid').isotope({
                layoutMode: 'packery',
                itemSelector: '.Grid-item',
                packery: {
                     gutter: 10
                },
                getSortData: {
                     pro: '[data-pro]',
                     player: '[data-player]',
                     noob: '[data-noob]'
                },
                sortBy: 'noob'
              });

              // bind filter button click
              $('#levelMap').on('click', 'a', function () {
                var sortBy = $(this).attr('data-sort');

                $grid.isotope({
                     sortBy: sortBy,
                     sortAscending: true
                });
              });
           });

        </script>


        <script>

          (function(){
              var cookieName = 'persist-policy';
              var cookieValue = 'eu';
              var createCookie = function(name,value,days,domain) {
                  var expires = '';
                  var verifiedDomain = '';
                  if (days) {
                      var date = new Date();
                      date.setTime(date.getTime()+(days*24*60*60*1000));
                      expires = '; expires='+date.toGMTString();
                  }
                  if (domain) {
                      verifiedDomain = '; domain='+domain;
                  }
                  document.cookie = name+'='+value+expires+verifiedDomain+'; path=/';
              };
              var readCookie = function(name) {
                  var nameEQ = name + "=";
                  var ca = document.cookie.split(';');
                  for(var i=0;i < ca.length;i++) {
                      var c = ca[i];
                      while (c.charAt(0)==' ') c = c.substring(1,c.length);
                      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                  }
                  return null;
              };

              window.initPolicyWidget = function(){
                  jQuery('#policyNotice').show().find('.close-btn a').click(function(e){
                      createCookie(cookieName, cookieValue, 180, jQuery(this).data('domain'));
                      jQuery('#policyNotice').remove();
                      return false;
                  });
              }
              var cookieContent = readCookie(cookieName);
              if (typeof  cookieContent === 'undefined' || cookieContent != cookieValue) {
                  jQuery(document).ready(initPolicyWidget);
                  jQuery(document).trigger('policyWidgetReady');
              }
          })();

      </script>


        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-15279170-1']);
            _gaq.push(['_trackPageview', 'fallout_4_tracking']);

            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
      </div>
    </div>
  </body>
</html>
