<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Terms &amp; Conditions</title>
    <link rel="shortcut icon" href="dist/build/img/favicon.png">
    <link rel="apple-touch-icon" href="dist/build/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="dist/build/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="dist/build/img/apple-touch-icon-114x114.png">
    <link rel="stylesheet" href="dist/build/css/style.css">

    <script src="https://use.typekit.net/rqb4eew.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
  </head>
  <body class="terms">

      <div class="Page">
        <div class="Page-entry">
          <div class="Center Center-height">

            <h2 class="tk-futura-pt">GAME OF CHANCE PROMOTION</h2>
            <h3 class="tk-futura-pt">TERMS &amp; CONDITIONS</h3>

            </br>
            </br>

            <p>
                1. By participating in the Fallout 4 Promotion (the "Promotion") promoted by Ziff Davis International Ltd. (@IGNUK) (the "Promoter"), you agree to
                these Official Terms &amp; Conditions (the “Terms and Conditions”). When you enter the Promotion, these Terms and Conditions become a contract between you
                and the Promoter, so read them carefully before participating. This contract includes, without limitation, an indemnification by you of the released
                parties listed below and a limitation of your rights and remedies.
            </p>
            <p>
                <br/>
                2. The promoter of the Promotion is Ziff Davis International Ltd., 18 Mansell Street, London, E1 8AA (the “Promoter”).
            </p>
            <p>
                3. Entry is open to users who are legally resident in the United Kingdom and are eighteen (18) years of age or over. Entrants who are eligible to enter the
                Promotion pursuant to these Terms and Conditions are referred to as “Eligible Entrants”.
            </p>
            <p>
                <br/>
                4. Directors, management, employees and officers of the Promoter and its related corporate entities (such as parent and subsidiary companies), as well as
                employees of associated agencies involved in the Promotion and their immediate families are ineligible to enter this Promotion.
            </p>
            <p>
                5. The Promotion commences 12pm (GMT) on 24 March 2016, and closes at 10am (GMT) on 21 April, 2016 (the "Promotion Period”).
            </p>
            <p>
                6. To enter the Promotion, entrants must answer the question and enter their details found at http://uk-microsites.ign.com/fallout-4. Subsequent attempts
                made by the same individual to submit multiple entries by using multiple accounts or otherwise may result in disqualification of the entrant.
            </p>
            <p>
                Entries submitted by any other means other than by filling out the specified survey will not be accepted. Any illegible, incomplete or fraudulent entries
                will be rejected. Only one entry per person will be accepted. Use of script, macro or any automated system to enter the Promotion is prohibited and entries
                made (or which appear to have been made, in Promotor’s sole discretion) using any such system may be treated as void. All entries become the property of
                the Promoter. Participants must follow the directions provided by the Promoter and otherwise be bound by and follow these Terms and Conditions to be
                eligible to receive a Prize in connection with the Promotion.
                <strong>
                    Participants may not take actions that are negligent, injurious or reckless in pursuit of any prize offered in connection with the Promotion.
                    Participants who do not follow this rule or who otherwise participate in the Promotion in an unsportsmanlike manner will be disqualified from the
                    Promotion. Participants alone are responsible for any injuries or other damages that are caused or incurred as a result of their behavior.
                </strong>
            </p>
            <p>
                7. Promoter will determine the giveaway recipients in its sole and absolute discretion in accordance with these Terms and Conditions. Each Prize recipient
                will be selected and contacted by Promoter. If the Promotion prize recipient is determined to be ineligible to receive the Prize, Promoter may, in its sole
                and absolute discretion, not give away the goods or may give the goods to the next Eligible Entrant. If you are selected as a prize recipient, you will be
                required to privately (by email or other secure, private means) provide your name, address, age and other personal information necessary to receive the
                Prize. The name, contact number and address of each prize recipient will be passed to a third party provider for delivery. Except for Winner Information
                (defined below), Promoter will not keep or store your information or use it for any purpose other than sending the Prize or providing you information on
                receiving the Prize. Promotions are subject to all applicable laws, rules or regulations and these Terms and Conditions.
            </p>
            <p>
                8. The winning entries will be drawn at random on 12 April, 2016 from all complete, eligible, properly submitted entries received by Promoter during the
                Promotion Period. The first entrant whose entry is drawn from the eligible entries shall win the Prize described below. There will be only one prize
                recipient for this Promotion. The Prize Winner will be notified by email on 06 April, 2016, and the name and locality of the Prize Winner will be announced
                on <a href="http://www.twitter.com/ignuk">www.twitter.com/ignuk</a>. If a Prize Winner does not reply to the notification email acknowledging acceptance of
                the Prize by 19 April, 2016, such Prize Winner will forfeit the Prize. In such instance, the Promoter reserves the right to award the Prize to another
                Eligible Entrant.
            </p>
            <p>
                9. The winning entrant (the “Prize Winner”) will receive the following prize(s) (collectively, the “Prize”): One (1) Fallout 4 Season Pass on a choice of
                either Xbox One, Playstation 4, or PC, estimated retail value thirty nine pounds ninety nine pence sterling (£39.99), one (1) Sub-Pac M2, estimated retail
                value two hundred and forty three pounds sterling (£243), one (1) Fallout 4 t-shirt, estimated retail value twenty pounds sterling (£20), and one (1)
                Fallout 4 hat, estimated retail value ten pounds sterling (£10).
            </p>
            <p>
                Two runners-up will receive One (1) Fallout 4 Season Pass on a choice of either Xbox One, Playstation 4, or PC, estimated retail value thirty nine pounds
                ninety nine pence sterling (£39.99), one (1) Fallout 4 t-shirt, estimated retail value twenty pounds sterling (£20), and one (1) Fallout 4 hat, estimated
                retail value ten pounds sterling (£10).
            </p>
            <p>
                Total estimated retail value of the Prize is four hundred and fifty one pounds ninety eight pence sterling (£451.98).
            </p>
            <p>
                10. The Prize must be accepted by the Prize Winner as stated and cannot be transferred to another person, exchanged for other goods and services or
                redeemed as cash in whole or in part. Any element of a Prize which is not accepted will be forfeited, and no compensation will be paid in lieu of that
                element of the Prize. Should a Prize be unavailable at the time that the Promoter seeks to acquire the Prize for the Prize Winner, Promoter reserves the
                right, in its sole and absolute discretion, to substitute with an alternative of equal or greater value or to substitute a cash alternative for any Prize.
                You agree that the Prize is awarded on an “as is” basis and that neither the Promoter nor any of its parents, subsidiaries or affiliated companies make any
                representations or warranties of any nature with respect to the Prize. Each Prize Winner in the Promotion is solely responsible for any and all applicable
                taxes (including income and withholding taxes on any Prize), regardless of whether the Prize, in whole or in part, is used. The actual retail value of any
                Prize is based on available information provided to Promoter and the value of any Prize awarded to a Prize Winner must be reported for tax purposes as
                required by law. Prizes may subject to the terms and conditions of a third party prize provider which will be made available by such third party; in such
                instance, any complaints or queries relating to the use of the Prize should be directed to such third party prize provider.
            </p>
            <p>
                11. Except for any liability for death or personal injury caused by its negligence, fraud or any other liability that cannot be excluded by law, the
                Promoter and its parent companies, subsidiaries, affiliated companies, units and divisions, and the current and former officers, directors, employees,
                shareholders, agents, successors and assigns of each of the foregoing, and all those acting under the authority of the foregoing or any of them (including,
                but not limited to, advertising and promotional agencies and prize suppliers) (collectively, the "Released Parties") exclude all liability, claims,
                actions, injury, loss, damages, liabilities and obligations of any kind whatsoever (collectively, “Claims”), whether known or unknown, suspected or
                unsuspected, which entrants have ever had, now have, or hereafter can, shall or may have, against the Released Parties (or any of them), including, but not
                limited to, all Claims for personal injury, theft, unauthorised access or third party interference arising from or related to: (a) the Promotion; and (b)
                the receipt, ownership, use, transfer, sale or other disposition of the Prize, including, but not limited to, claims for personal injury, death, and/or
                property damages. Prize Winner shall be solely responsible for any tax liability incurred in connection with the Prize.
            </p>
            <p>
                12. The Promoter accepts no responsibility for any variation of the Prize or any aspect of the Prize, due to circumstances outside its reasonable control.
                In any such event, an alternative prize or element of the Prize will be arranged.
            </p>
            <p>
                13. The Promoter is not responsible for any problem or technical malfunction of any website or communications network or any late, lost, incorrectly
                submitted, delayed, ineligible, incomplete, corrupted or misdirected entry whether due to error, transmission interruption or otherwise. The Promoter
                reserves the right to disqualify any entrant submitting an entry which, in the opinion of the Promoter, includes objectionable content, including but not
                limited to profanity, nudity, potentially insulting, scandalous, inflammatory or defamatory images or language. The Promoter’s decision is final.
            </p>
            <p>
                <br/>
                14. If, for any reason, the Promotion is not capable of running as planned, including, but not limited to, infection by computer virus, bugs, tampering,
                unauthorised intervention, fraud, technical failures or any other causes beyond the reasonable control of the Promoter which corrupt or affect the
                administration security, fairness, integrity or proper conduct of this Promotion, the Promoter reserves the right in its sole discretion to disqualify any
                individual who tampers with the entry process, and/or to cancel, terminate, modify or suspend the Promotion. The Promoter assumes no responsibility for any
                error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorised
                access to, or alteration of, entries beyond its reasonable control. The Promoter is not responsible for any problems or technical malfunction of any
                telephone network or lines, computer online systems, servers or providers, computer equipment, software, failure of any entry to be received by the
                Promoter on account of technical problems or traffic congestion on the Internet or at any website, or any combination thereof, including any injury or
                damage to participant's or any other person's computer related to or resulting from participation or downloading any materials in this Promotion. CAUTION:
                ANY ATTEMPT TO DELIBERATELY DAMAGE ANY WEBSITE OR THE INFORMATION ON A WEBSITE, OR TO OTHERWISE UNDERMINE THE LEGITIMATE OPERATION OF THIS PROMOTION, MAY
                BE A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, WHETHER SUCCESSFUL OR NOT, THE PROMOTER RESERVES THE RIGHT TO SEEK DAMAGES TO
                THE FULLEST EXTENT PERMITTED BY LAW.
            </p>
            <p>
                <br/>
                15. The Prize Winner, by acceptance of the Prize, grants to Promoter and its parents, subsidiaries, affiliates, designees and assigns the right to
                publicize his or her name, address (city and country of residence), photograph, voice, statements and/or other likeness and prize information for
                advertising, promotional and/or trade and/or any other purpose ("Winner Information") in any media or format now known or hereafter devised, throughout the
                world, in perpetuity, without limitation and without further compensation, consideration, permission or notification, except where prohibited by law. The
                Prize Winner agrees to participate in reasonable publicity activities surrounding the Promotion as requested by the Promoter.
            </p>
            <p>
                16. Without limiting the foregoing, the personal information provided by entrants in connection with this Promotion will be handled in accordance with data
                protection legislation and in accordance with the Promoter’s privacy policy, which may be found <a href="http://www.ziffdavis.com/privacy-policy">here</a>.
            </p>
            <p>
                17. These Terms and Conditions are governed by the laws of England and Wales. The courts located in London, England shall have exclusive jurisdiction to
                hear any dispute or claim arising in association with the Promotion or these Terms and Conditions.
            </p>
            <p>
                18. Although the Promotion may be featured on Twitter/Facebook, the Promotion is in no way sponsored, endorsed, administered by, or association with
                Twitter/Facebook and you agree that Twitter/Facebook are not liable in any way for any claims, damages or losses associated with the Promotion. You
                acknowledge that when you submit an entry or other correspondence in connection with the Promotion, are providing information to the Promoter and not to
                Twitter/Facebook.
            </p>
            <p>
                19. A list of Promotion winners can be made available on request by writing to the offices of the Promoter: Ziff Davis International Ltd., 18 Mansell
                Street, London, E1 8AA. A stamped addressed envelope must be included with the request.
            </p>

        </div>
      </div>
     </div>

  </body>
</html>
